package event_retriever

import (
	"compressed-allocations/src-go/stakers/internal/types"
)

type EventsRetriever interface {
	BatchQueryEvents(start uint64, end uint64, txWorkerPoolSize int)
	GetStakers() *types.DepositorMap
}
