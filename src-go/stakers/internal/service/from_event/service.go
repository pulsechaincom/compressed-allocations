package from_event

import (
	"compressed-allocations/src-go/stakers/generated/deposit_contract"
	"compressed-allocations/src-go/stakers/internal/config"
	"compressed-allocations/src-go/stakers/internal/service/from_event/event_retriever"
	"compressed-allocations/src-go/stakers/internal/types"
	"compressed-allocations/src-go/utils"

	log "github.com/sirupsen/logrus"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/gammazero/workerpool"
)

type service struct {
	eventsRetriever event_retriever.EventsRetriever
	cfg             *config.Config
}

func NewService(client *ethclient.Client, contract *deposit_contract.DepositContract, cfg *config.Config) *service {
	log.Infof("config: %v", cfg)

	return &service{
		cfg:             cfg,
		eventsRetriever: event_retriever.New(client, contract),
	}
}

func (svc *service) GetAllStakers(beginningBlockNumber, endingBlockNumber uint64) *types.DepositorMap {
	numBlocks := endingBlockNumber - beginningBlockNumber
	wp := workerpool.New(svc.cfg.BatchWorkerPoolSize)
	var work func()

	bar := utils.InitProgressBar(int(numBlocks), int(svc.cfg.EventsBatchSize), "All Stakers")

	// Launch filter queries in worker-pool
	for start := beginningBlockNumber; start < endingBlockNumber; start += svc.cfg.EventsBatchSize {
		// we do new memory allocation in each iteration
		// we pass this variable in `func() {}` which will be executed concurrently in work pool
		workStartsAt := start
		workEndsAt := start + svc.cfg.EventsBatchSize - 1
		if workEndsAt > endingBlockNumber {
			workEndsAt = endingBlockNumber
		}

		work = func() {
			svc.eventsRetriever.BatchQueryEvents(workStartsAt, workEndsAt, svc.cfg.TxWorkerPoolSize)
			bar.Add(1)
		}

		wp.Submit(work)
	}

	// Await Worker Pool to finish
	wp.StopWait()

	return svc.eventsRetriever.GetStakers()
}
