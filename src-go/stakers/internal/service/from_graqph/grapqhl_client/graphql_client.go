package grapqhl_client

import (
	"context"
	"fmt"
	"strconv"

	"compressed-allocations/src-go/stakers/internal/types"

	"github.com/machinebox/graphql"
)

type client struct {
	graphqlClient *graphql.Client
}

func NewClient(address string) *client {
	return &client{
		graphqlClient: graphql.NewClient(address),
	}
}

func (c *client) GetTotalDepositorsAmount() (int, error) {
	graphqlRequest := graphql.NewRequest(`
		{
		  aggregations(first: 1) {
			totalDepositors
		  }
		}
    `)

	var graphqlResponse struct {
		Aggregations []struct {
			TotalDepositors string `json:"totalDepositors"`
		} `json:"aggregations"`
	}

	err := c.graphqlClient.Run(context.Background(), graphqlRequest, &graphqlResponse)
	if err != nil {
		return 0, err
	}

	return strconv.Atoi(graphqlResponse.Aggregations[0].TotalDepositors)
}

// GetStakingDataByBunch returns list of depositors in pagination manner
func (c *client) GetStakingDataByBunch(limit int, after string) ([]types.Depositor, error) {
	graphqlRequest := graphql.NewRequest(fmt.Sprintf(`
		{
		  depositors(first: %v, where: { id_gt: "%s" }) {
			id
			totalAmountDeposited
		  }
		}
    `, limit, after))

	var graphqlResponse struct {
		Depositors []types.Depositor `json:"depositors"`
	}

	err := c.graphqlClient.Run(context.Background(), graphqlRequest, &graphqlResponse)
	if err != nil {
		return nil, err
	}

	return graphqlResponse.Depositors, nil
}
